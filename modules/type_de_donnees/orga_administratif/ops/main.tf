resource "aws_iam_role" "iam_for_lambda" {
  name = "iam_for_lambda"
  tags = {
      stage = "collecte"
    }
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    },
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "s3.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

/* Lambda de colle des données sur le découpage administration de la france. 
    les données collectées par cette lambda sont stockées dans une base de 
    données RDS de type Aurora Mysql */

resource "aws_lambda_function" "organisation_administrative" {
  filename      = "${path.module}/lambda_function.zip"
  function_name = "organisation_administrative"
  role          = aws_iam_role.iam_for_lambda.arn
  handler       = "lambda_function.lambda_handler"
  runtime = "python3.8"
  layers = [aws_lambda_layer_version.request.arn]
  memory_size = 128
  timeout = 900
  tags = {
      stage = "collecte"
    }
}

resource "aws_lambda_layer_version" "request" {
  filename   = "${path.module}/requests.zip"
  layer_name = "request"
  compatible_runtimes = ["python3.6", "python3.7", "python3.8"]
}

/*
Table dynamo pour la sauvegarde des données sur l'organisation administrative
*/

resource "aws_dynamodb_table" "organisation_administrative" {
  name           = "organisation_administrative"
  #billing_mode   = "PROVISIONED"
  billing_mode   = "PAY_PER_REQUEST"
  #read_capacity  = 20
  #write_capacity = 20
  hash_key       = "code_commune"

  attribute {
    name = "code_commune"
    type = "S"
  }

  ttl {
    attribute_name = "TimeToExist"
    enabled        = false
  }
  
  tags = {
    stage = "commun"
  }
}
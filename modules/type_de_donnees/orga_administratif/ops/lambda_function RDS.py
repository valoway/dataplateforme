import json, requests, time,  boto3

rds_client = boto3.client('rds-data')
database_name = 'bd_aurora_valoway'
db_cluster_arn = 'arn:aws:rds:eu-west-3:499645128731:cluster:valoway-aurora'
db_secret_manager_arn = 'arn:aws:secretsmanager:eu-west-3:499645128731:secret:valoway-rds-pWcOvP'

def lambda_handler(event, context):
    decoupage_admin = dict(
        {"code_reg": [], "code_dep": [], "nom_dep": [], "code_commune": [], "nom_commune": [], "codesPostaux": [],
         "longitude": [], "latitude": [], "population": []})  # ,"population": []
    region = {
        'key': ['01', '02', '03', '04', '06', '11', '24', '27', '28', '32', '44', '52', '53', '75', '76', '84', '93',
                '94', 'COM'],
        'nom': ['Guadeloupe', 'Martinique', 'Guyane', 'La Réunion', 'Mayotte', 'Île-de-France', 'Centre-Val de Loire',
                'Bourgogne-Franche-Comté', 'Normandie', 'Hauts-de-France',
                'Grand Est', 'Pays de la Loire', 'Bretagne', 'Nouvelle-Aquitaine', 'Occitanie', 'Auvergne-Rhône-Alpes',
                'Provence-Alpes-Côte d''Azur', 'Corse', 'Collectivités d''Outre-Mer']
    }

    for item in region["key"]:
        print(item)
        url = "https://geo.api.gouv.fr/departements?codeRegion={0}&fields=nom,code,codeRegion".format(item)
        response = requests.get(url)
        content = response.json()
        for dep in content:
            # print("je suis entrée dans le deuxième boucle")
            nom_dep = dep["nom"]
            code_dep = dep["code"]
            code_reg = dep["codeRegion"]

            url = "https://geo.api.gouv.fr/communes?codeDepartement={0}&fields=nom,code,codesPostaux,population&format=geojson&geometry=centre".format(
                code_dep)
            res_com = requests.get(url)
            content_com = res_com.json()
            for com in content_com["features"]:
                # print("deuxieme boucle for")
                code_com = com["properties"]["code"]
                nom_com = com["properties"]["nom"]
                codesPostaux = com["properties"]["codesPostaux"][0]
                if "population" in com["properties"]:
                    population = com["properties"]["population"]
                else:
                    population = "0"
                longitude = com["geometry"]["coordinates"][0]
                latitude = com["geometry"]["coordinates"][1]

                decoupage_admin["code_reg"].append(code_reg)
                decoupage_admin["code_dep"].append(code_dep)
                decoupage_admin["nom_dep"].append(nom_dep)
                decoupage_admin["code_commune"].append(code_com)
                decoupage_admin["nom_commune"].append(nom_com)
                decoupage_admin["codesPostaux"].append(codesPostaux)
                decoupage_admin["population"].append(population)
                decoupage_admin["longitude"].append(longitude)
                decoupage_admin["latitude"].append(latitude)

                requete = "insert into organisation_administrative(code_reg, code_dep, nom_dep, code_commune, nom_commune, code_postaux, population, longitude, latitude) values ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}')".format(
                    code_reg, code_dep, nom_dep.replace("'","''",1), code_com, nom_com.replace("'","''",1), codesPostaux, population, longitude, latitude)
                
                try:
                    insertion(requete)
                except Exception as e:
                    print("***** ", "Exception levee", e)
                    print("***** Code commune concernee *****", code_com)

        s3 = boto3.resource('s3')
        s3object = s3.Object('valoway-backend-tfstate-dev', 'decoupage_admin.json')

    s3object.put(
        Body=(bytes(json.dumps(decoupage_admin).encode('UTF-8')))
    )
    return {
        'statusCode': 200,
        'body': len(decoupage_admin['code_reg'])
    }


def insertion(sql):
    print("***** Excecution de la requete ", sql, " ******")
    response = rds_client.execute_statement(
        secretArn = db_secret_manager_arn,
        database = database_name,
        resourceArn = db_cluster_arn,
        sql = sql
      )
import json, requests, time, boto3

dynamo = boto3.resource('dynamodb')
table = dynamo.Table('organisation_administrative')

def lambda_handler(event, context):
    region = {
        'key': ['01', '02', '03', '04', '06', '11', '24', '27', '28', '32', '44', '52', '53', '75', '76', '84', '93',
                '94', 'COM'],
        'nom': ['Guadeloupe', 'Martinique', 'Guyane', 'La Réunion', 'Mayotte', 'Île-de-France', 'Centre-Val de Loire',
                'Bourgogne-Franche-Comté', 'Normandie', 'Hauts-de-France',
                'Grand Est', 'Pays de la Loire', 'Bretagne', 'Nouvelle-Aquitaine', 'Occitanie', 'Auvergne-Rhône-Alpes',
                'Provence-Alpes-Côte d''Azur', 'Corse', 'Collectivités d''Outre-Mer']
    }

    for item in region["key"]:
        print(item)
        url = "https://geo.api.gouv.fr/departements?codeRegion={0}&fields=nom,code,codeRegion".format(item)
        response = requests.get(url)
        content = response.json()
        for dep in content:
            # print("je suis entrée dans le deuxième boucle")
            nom_dep = dep["nom"]
            code_dep = dep["code"]
            code_reg = dep["codeRegion"]

            url = "https://geo.api.gouv.fr/communes?codeDepartement={0}&fields=nom,code,codesPostaux,population&format=geojson&geometry=centre".format(
                code_dep)
            res_com = requests.get(url)
            content_com = res_com.json()
            for com in content_com["features"]:
                # print("deuxieme boucle for")
                code_com = com["properties"]["code"]
                nom_com = com["properties"]["nom"]
                codesPostaux = com["properties"]["codesPostaux"][0]
                if "population" in com["properties"]:
                    population = com["properties"]["population"]
                else:
                    population = "0"
                longitude = com["geometry"]["coordinates"][0]
                latitude = com["geometry"]["coordinates"][1]

                commune = {}
                commune["code_reg"] = code_reg
                commune["code_dep"] = code_dep
                commune["nom_dep"] = nom_dep
                commune["code_commune"] = code_com
                commune["nom_commune"] = nom_com
                commune["codesPostaux"] = codesPostaux
                commune["population"] = population
                commune["longitude"] = str(longitude)
                commune["latitude"] = str(latitude)

                try:
                    table.put_item(Item=commune)
                except Exception as e:
                    print("***** ", "Exception levee", e)
                    print("***** Code commune concernee *****", code_com)

    return {
        'statusCode': 200,
        'body': "Terminee"
    }

resource "random_password" "master"{
  length           = 4
  special          = false
  override_special = "_"
}

# resource "aws_rds_cluster" "valoway_rds_aurora" {
#   cluster_identifier      = "valoway-rds-aurora"
#   engine                  = "aurora-mysql"
#   engine_version          = "5.7.mysql_aurora.2.03.2"
#   #engine_mode = "serverless"
#   enable_http_endpoint = true
#   availability_zones      = ["eu-west-3a", "eu-west-3b", "eu-west-3c"]
#   database_name           = "bd_aurora_valoway"
#   master_username         = "admin"
#   master_password         = "Valoway-AWS2021!"
#   copy_tags_to_snapshot   = true
#   skip_final_snapshot = false
#   final_snapshot_identifier = "snapshot"
#   backup_retention_period = 5
#   preferred_backup_window = "07:00-09:00"
#   tags = {
#       stage = "commun"
#     }
# }

# resource "aws_rds_cluster_instance" "cluster_instances" {
#   count              = 1
#   identifier         = "valoway-rds-aurora-${count.index}"
#   cluster_identifier = aws_rds_cluster.valoway_rds_aurora.id
#   instance_class     = "db.t3.small"
#   engine             = aws_rds_cluster.valoway_rds_aurora.engine
#   engine_version     = aws_rds_cluster.valoway_rds_aurora.engine_version
#   copy_tags_to_snapshot   = true
#   publicly_accessible = true
#   #writer = true
#   tags = {
#       stage = "commun"
#     }
# }


resource "aws_rds_cluster" "valoway_rds_aurora_ss" {
  cluster_identifier      = "valoway-rds-aurora-ss"
  engine                  = "aurora-mysql"
  engine_version          = "5.7.mysql_aurora.2.03.2"
  engine_mode = "serverless"
  enable_http_endpoint = true
  availability_zones      = ["eu-west-3a", "eu-west-3b", "eu-west-3c"]
  database_name           = "bd_aurora_valoway"
  master_username         = "admin"
  master_password         = "Valoway-AWS2021!"
  copy_tags_to_snapshot   = true
  skip_final_snapshot = false
  final_snapshot_identifier = "valoway-rds-aurora-ss"
  backup_retention_period = 5
  preferred_backup_window = "01:00-09:00"
  tags = {
      stage = "commun"
  }
}

# resource "aws_secretsmanager_secret" "rotation-example" {
#   name                = "rotation-example"
# }
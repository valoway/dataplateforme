CREATE TABLE `bd_aurora_valoway`.`organisation_administrative` (
  `code_reg` VARCHAR(45) NULL,
  `code_dep` VARCHAR(45) NULL,
  `nom_dep` VARCHAR(250) NULL,
  `code_commune` VARCHAR(45) not NULL,
  `nom_commune` VARCHAR(250) NULL,
  `code_postaux` VARCHAR(45) NULL,
  `longitude` VARCHAR(45) NULL,
  `latitude` VARCHAR(45) NULL,
  `population` int NULL,
  PRIMARY KEY (`code_commune`));


insert into `bd_aurora_valoway`.`organisation_administrative` (code_commune) value("10");
commit;
select * from `bd_aurora_valoway`.`organisation_administrative`;
;

  --`id` INT NOT NULL AUTO_INCREMENT,
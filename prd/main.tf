module "iam" {
  source = "../modules/transversales/iam"
}

module "rds" {
  source = "../modules/transversales/rds"
}

module "organisation_administrative" {
  source = "../modules/type_de_donnees/orga_administratif/ops"
}
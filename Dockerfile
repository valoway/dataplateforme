##Grab the latest python image
FROM python:3.8
MAINTAINER Valoway

## Install python and pip
RUN apt-get update -y \
    && apt-get install apt-utils -y \
    && apt-get upgrade -qy \
    && apt-get install tree \
    && apt-get install -qy awscli \
    && apt-get install -qy git \
    && apt-get install -qy zip

################################
# Install Terraform
################################
# Download terraform for linux
RUN wget https://releases.hashicorp.com/terraform/1.0.0/terraform_1.0.0_linux_amd64.zip \
    && unzip terraform_1.0.0_linux_amd64.zip \
    && mv terraform /usr/local/bin/ \
    && terraform --version \
    && git --version

################################
# End Install Terraform
################################

ADD ./requirements.txt /tmp/requirements.txt

# Install dependencies
RUN pip install -r /tmp/requirements.txt

ADD . ./data
WORKDIR ./data
RUN ls -l

provider "aws" {
  #version = "~> 3.48.0"
  region                  = "eu-west-3"
  shared_credentials_file = "./credentials"
  default_tags {
    tags = {
      objectif      = "projet"
      client        = "valoway"
      objectif_name = "forkast"
    }
  }
}

terraform {
  backend "s3" {
    bucket                  = "valoway-backend-tfstate-dev"
    key                     = "dev.tfstate"
    region                  = "eu-west-3"
    shared_credentials_file = "./credentials"
    dynamodb_table          = "dev.tfstate"
  }
}